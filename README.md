# Accounting Setup

## How to run the API app
1. Go to directory `~/accounting-notebook/bin/api`
2. Double click on `AccountingNotebook.Api.exe` to execute

## How to run the Client app
1. Run cmd.exe as administrator
2. Go to directory `~/accounting-notebook/bin/client`
3. Execute `npm install -g serve` to install simple http server
4. Execute `serve -s -l 5002` to deploy client app
5. Open browser and navigate to http://localhost:5002

# Consuming API
Base URL: http://localhost:5000/api

## Account Balance
Description: Endpoint retrieves current account balance  
URL: `/transactions/current-balance`  
Method: `GET`

## Transactions
### Fetch transactions history
Description: Endpoint retrieves transactions history  
URL: `/transactions`  
Method: `GET` 

### Find transaction by id
Description: Endpoint retrieves transaction by specified id. Id is string representation of Guid  
URL: `/transactions/{id}`  
Method: `GET`  

### Commit transaction
Description: Endpoint commit new transaction to the account  
URL: `/transactions`  
Method: `POST`  
Headers: `Content-Type: application/json`  
Body: `{ type: 'credit|debit', amount: 0.0 }` 