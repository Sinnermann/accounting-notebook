﻿using AccountingNotebook.Models;

namespace AccountingNotebook.ApiModels
{
    public class TransactionDto
    {
        public TransactionType Type { get; set; }

        public decimal Amount { get; set; }
    }
}
