﻿using AccountingNotebook.ApiModels;
using AccountingNotebook.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace AccountingNotebook.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        private readonly IAccountsService _accountsService;
        private readonly ITransactionsService _transactionsService;

        public TransactionsController(
            IAccountsService accountsService,
            ITransactionsService transactionsService)
        {
            _accountsService = accountsService;
            _transactionsService = transactionsService;
        }

        [HttpGet("current-balance")]
        public IActionResult GetCurrentBalance()
        {
            var account = _accountsService.GetCurrentAccount();
            return Ok(new { account.Balance });
        }


        [HttpGet]
        public IActionResult GetAll()
        {
            var transactions = _transactionsService.GetAll();
            if (transactions is null || transactions.Count == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(transactions);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(string id)
        {
            var transaction = _transactionsService.GetById(id);
            if (transaction is null)
            {
                return NotFound();
            }
            else
            {
                return Ok(transaction);
            }
        }

        [HttpPost]
        public IActionResult Commit([FromBody] TransactionDto transaction)
        {
            _transactionsService.CommitTransaction(transaction.Type, transaction.Amount);
            return Ok();
        }
    }
}
