﻿using AccountingNotebook.Models;

namespace AccountingNotebook.Interfaces
{
    public interface IAccountRepository
    {
        Account Get();

        void Update(Account account);
    }
}
