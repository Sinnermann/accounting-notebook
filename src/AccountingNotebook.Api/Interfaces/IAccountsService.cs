﻿using AccountingNotebook.Models;

namespace AccountingNotebook.Interfaces
{
    public interface IAccountsService
    {
        Account GetCurrentAccount();
    }
}
