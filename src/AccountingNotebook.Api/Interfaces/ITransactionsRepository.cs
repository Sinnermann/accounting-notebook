﻿using System.Collections.Generic;
using AccountingNotebook.Models;

namespace AccountingNotebook.Interfaces
{
    public interface ITransactionsRepository
    {
        void Add(TransactionType type, decimal amount);

        List<Transaction> GetAll();

        Transaction GetById(string transactionId);
    }
}
