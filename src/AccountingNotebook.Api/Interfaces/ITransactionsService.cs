﻿using System.Collections.Generic;
using AccountingNotebook.Models;

namespace AccountingNotebook.Interfaces
{
    public interface ITransactionsService
    {
        void CommitTransaction(TransactionType transactionType, decimal amount);

        List<Transaction> GetAll();

        Transaction GetById(string transactionId);
    }
}
