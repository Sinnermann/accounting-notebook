﻿using System;

namespace AccountingNotebook.Models
{
    public class Account : ICloneable
    {
        public decimal Balance { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public void SetBalance(TransactionType transactionType, decimal amount)
        {
            if (transactionType == TransactionType.Credit)
            {
                Balance += amount;
            }
            else if (transactionType == TransactionType.Debit)
            {
                Balance -= amount;
            }
            else
            {
                throw new ArgumentException("Invalid Transaction Type");
            }
        }
    }
}
