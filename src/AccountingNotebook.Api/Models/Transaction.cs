﻿using System;

namespace AccountingNotebook.Models
{
    public class Transaction
    {
        public string Id { get; set; }

        public TransactionType Type { get; set; }

        public decimal Amount { get; set; }

        public DateTime EffectiveDate { get; set; }
    }
}
