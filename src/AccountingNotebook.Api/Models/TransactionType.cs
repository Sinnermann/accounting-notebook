﻿namespace AccountingNotebook.Models
{
    public enum TransactionType
    {
        Credit,
        Debit
    }
}