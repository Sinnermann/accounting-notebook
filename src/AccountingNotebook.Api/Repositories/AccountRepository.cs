﻿using AccountingNotebook.Interfaces;
using AccountingNotebook.Models;

namespace AccountingNotebook.Repositories
{
    public class AccountRepository : IAccountRepository
    {
        private Account _account = new Account();

        public void Update(Account account)
        {
            _account = account;
        }

        public Account Get()
        {
            return _account.Clone() as Account;
        }
    }
}
