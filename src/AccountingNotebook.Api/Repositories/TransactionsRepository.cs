﻿using System;
using System.Collections.Generic;
using System.Linq;
using AccountingNotebook.Interfaces;
using AccountingNotebook.Models;

namespace AccountingNotebook.Repositories
{
    public class TransactionsRepository : ITransactionsRepository
    {
        private List<Transaction> _transactions = new List<Transaction>();

        public List<Transaction> GetAll()
        {
            return _transactions;
        }

        public Transaction GetById(string transactionId)
        {
            return _transactions.FirstOrDefault(t => t.Id == transactionId);
        }

        public void Add(TransactionType type, decimal amount)
        {
            var transaction = new Transaction
            {
                Id = Guid.NewGuid().ToString(),
                Type = type,
                Amount = amount,
                EffectiveDate = DateTime.UtcNow
            };

            _transactions.Add(transaction);
        }
    }
}
