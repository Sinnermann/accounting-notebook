﻿using AccountingNotebook.Interfaces;
using AccountingNotebook.Models;

namespace AccountingNotebook.Services
{
    public class AccountsService : IAccountsService
    {
        private readonly IAccountRepository _accountRepository;

        public AccountsService(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        public Account GetCurrentAccount()
        {
            return _accountRepository.Get();
        }
    }
}
