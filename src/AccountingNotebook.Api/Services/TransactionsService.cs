﻿using System;
using System.Collections.Generic;
using System.Threading;
using AccountingNotebook.Interfaces;
using AccountingNotebook.Models;

namespace AccountingNotebook.Services
{
    public class TransactionsService : ITransactionsService
    {
        private ReaderWriterLockSlim _locker = new ReaderWriterLockSlim();

        private readonly IAccountRepository _accountRepository;
        private readonly ITransactionsRepository _transactionsRepository;

        public TransactionsService(
            IAccountRepository accountRepository,
            ITransactionsRepository transactionsRepository)
        {
            _accountRepository = accountRepository;
            _transactionsRepository = transactionsRepository;
        }

        public List<Transaction> GetAll()
        {
            _locker.EnterReadLock();

            try
            {
                return _transactionsRepository.GetAll();
            }
            finally
            {
                _locker.ExitReadLock();
            }
        }

        public Transaction GetById(string transactionId)
        {
            _locker.EnterReadLock();

            try
            {
                return _transactionsRepository.GetById(transactionId);
            }
            finally
            {
                _locker.ExitReadLock();
            }
        }

        public void CommitTransaction(TransactionType transactionType, decimal amount)
        {
            _locker.EnterWriteLock();

            try
            {
                var account = _accountRepository.Get();

                account.SetBalance(transactionType, amount);

                if (account.Balance < 0)
                {
                    throw new InvalidOperationException("The transaction cannot be committed because it will result in the negative account balance");
                }

                _accountRepository.Update(account);
                _transactionsRepository.Add(transactionType, amount);
            }
            finally
            {
                _locker.ExitWriteLock();
            }
        }
    }
}
