import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'TransactionsHistory',
    component: () => import('../views/transactions-history.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
