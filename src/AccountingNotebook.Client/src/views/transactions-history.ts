import { Vue, Component } from "vue-property-decorator";
import Transaction from '@/models/transaction';
import { transactionsService } from '@/services/transactions-service';

@Component
export default class TransactionsHistoryComponent extends Vue {

    private transactions: Transaction[] = [];

    getVariant(transaction: Transaction) {
        return transaction.type.toLowerCase() == "credit" ? "success" : "danger";
    }

    async mounted() {
        this.transactions = await transactionsService.GetTransactionsHistory();
    }
}